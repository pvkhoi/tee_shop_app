require 'open-uri'

module ApplicationHelper
	# Returns the full title on a per-page basis.
  def full_title(page_title = '')
    base_title = "Tee Shop"
    if page_title.empty?
      base_title
    else
      "#{page_title} | #{base_title}"
    end
  end

  def get_all_tees(url)
		html_content = open(url)
  	doc = Nokogiri::HTML(html_content)

    tee_css_selector = '.productIcon'

		all_tees = []
		tee_imgs = doc.css(tee_css_selector + ' .frame img')
		tee_imgs.each do |tee_img|
			all_tees << {src: tee_img.attr("data-original")}
		end

    tee_names = doc.css(tee_css_selector + ' .product-Title')
    for i in 0..all_tees.length-1
      all_tees[i][:name] = tee_names[i].text
    end

    tee_prices = doc.css(tee_css_selector + ' .product-price')
    for i in 0..all_tees.length-1
      all_tees[i][:price] = tee_prices[i].text
    end

    tee_url = doc.css(' .frame a')
    for i in 0..all_tees.length-1
      all_tees[i][:link] = tee_url[i].attr('href')
    end

    all_tees
	end
end
