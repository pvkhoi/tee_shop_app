class SearchController < ApplicationController
  include ApplicationHelper
  def index
  	@categories = Category.all
  	@search_query = search_params[:query]
    @all_tees = get_all_tees("http://www.sunfrogshirts.com/search/index.cfm?SEARCH=" + @search_query)
  end

  private
  	def search_params
  		params.permit(:query)
		end
end
