require 'open-uri'

class CategoriesController < ApplicationController
  include ApplicationHelper
  def show
    @categories = Category.all
  	@category = Category.find_by_url(id_params[:id])
  	url = @category.url
  	if url == "best-sellers"
  		url += ".cfm"
		end
    @all_tees = get_all_tees("http://www.sunfrogshirts.com/" + url)
  end

  private
  	def id_params
  		params.permit(:id)
		end
end
