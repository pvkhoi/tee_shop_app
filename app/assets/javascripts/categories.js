(function() {
	var setLazyLoading = function() {
		$('img.lazy').lazyload({
			effect: "fadeIn"
		});
	};

	var bind = function() {
		$(document).ready(function() {
			setLazyLoading();
		});
		// $('img.lazy').hover(function() {
		// 	var product = $(this).closest('.product');
		// 	$(product).find('.product-info').slideDown('fast', function() {
		// 	});
		// });
		// $('img.lazy').mouseleave(function() {
		// 	var product = $(this).closest('.product');
		// 	$(product).find('.product-info').slideUp('fast', function() {
		// 	});
		// });
	};

	bind();
	
})();